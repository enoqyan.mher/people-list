$( document ).ready(function() {
    $('#add').click(function () {
        if ($('#name').val() != '') {
            $('#sortable_list').show();
            var item = $('<li class="ui-state-default">' + $('#name').val() + '</li>');
            item.css("color", getRandomColor());
            $('#sortable_list').append(item);
            $('#name').val('');

            if ($('#sortable_list li').size() > 6) {
                $('#save').show();
            }
        }
    });

    $('#save').click(function () {
        var names_list = $('#sortable_list').clone();
        names_list.removeClass('col-md-8').css("border", "0 solid");
        names_list.find( "li" ).removeClass('ui-state-default');
        $('#content').html(names_list);
        $('#content').prepend('<h1 class="text-center">Names list</h1>');
    });


});

var colors = [];

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 15)];
    }

    if (colors.indexOf(color) != -1) {
        getRandomColor();
    }

    colors.push(color);
    return color;
}

$( function() {
    $( "#sortable_list" ).sortable();
    $( "#sortable_list" ).disableSelection();
} );